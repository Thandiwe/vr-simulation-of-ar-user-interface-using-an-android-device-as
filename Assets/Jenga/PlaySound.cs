﻿using System.Collections;
using UnityEngine;

public class PlaySound : MonoBehaviour {

    public AudioClip Soundtoplay;
    public float Volume;
    AudioSource sound;
    public bool alPlayed = false;
    void Start()
    {
        sound = GetComponent<AudioSource>();
    }

     void OnTriggerEnter()
        {
        if (!alPlayed)
            {
            sound.PlayOneShot(Soundtoplay, Volume);
                alPlayed = true;
 


            }
        }  	
	}
	
	