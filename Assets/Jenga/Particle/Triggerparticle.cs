﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggerparticle : MonoBehaviour
{

    public ParticleSystem _psystem;

    void start()
    {
        _psystem = GetComponent<ParticleSystem>();
    }

    void OnTriggerEnter(Collider col)
    {


        _psystem.Play();

    }

}
