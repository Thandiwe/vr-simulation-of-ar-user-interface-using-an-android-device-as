﻿#pragma strict
import System;
import System.IO;


var focused = false;
var selected = false;
var participantNumber = 0;


function Start () {
	
}

function writeFile(){
    // written to C:\Users\vrguys\AppData\LocalLow\DefaultCompany\ViveAndroidIntegration
    var fileName = Application.persistentDataPath + "/participant" + participantNumber + ".txt";
    if (File.Exists(fileName)){
        print(fileName + " already exists.");
        return;
    }
    var sr = File.CreateText(fileName);
    var targets = UsefulUtils.getSelectables(LayerMask.GetMask("Selectable"));
    for (target in targets){
        var selectable = target.GetComponent.<Selectable>();
        var results = selectable.retrieveResults();
        var keys = ["game object", "positional deviation", "rotational deviation", "rotation applied", "object selected times", "total object selected time"];
        for (var i = 0; i<results.length; i++){
            sr.WriteLine(keys[i] + ": " + results[i]);
        }
    }
    sr.Close();
}

function Update () {

}

function focus(anySelected){
    focused = true;
    if(! anySelected){
        var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.yellow;
    }
}

function unfocus(){
    focused = false;
    if(! selected){
        var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.white;
    }
}

function ApproveSelection(anySelected){
    if(focused && (!anySelected)){
        selected = true;
        var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.magenta;
        var rgBody = gameObject.GetComponent.<Rigidbody>();
        //Write down measurements, times ....
        writeFile();

    }
}

