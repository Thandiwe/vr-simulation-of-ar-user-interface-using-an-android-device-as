﻿
// Use this for initialization
function Start () {
}

// Update is called once per frame
function Update () {
}

static function FindGameObjectsWithLayer (layer : int) : GameObject[] {
     var goArray = FindObjectsOfType(GameObject);
     var goList = new System.Collections.Generic.List.<GameObject>();
     for (var i = 0; i < goArray.Length; i++) {
         if (goArray[i].layer == layer) {
             goList.Add(goArray[i]);
         }
     }
     if (goList.Count == 0) {
         //print("No GameObjects with layer: " + layer);
     }
     //print("Objects in Layer " + layer + " is " + goList.Count);
     return goList.ToArray();
 }

 static function getSelectables(layermask : LayerMask){
	var target_layerIndex = Mathf.RoundToInt(Mathf.Log(layermask.value, 2));
	var targets = UsefulUtils.FindGameObjectsWithLayer(target_layerIndex);
	return targets;
}

static function anySelected(layermask : LayerMask){
	var result = false;

	var targets = getSelectables(layermask);
	var layerIndex = Mathf.RoundToInt(Mathf.Log(layermask.value, 2));
	//print("Layerindex is: " + layerIndex);

	for (var j = 0; j < targets.Length; j++){
		if (layerIndex == 8) {
			var selectableBox : Selectable = targets[j].GetComponent(Selectable) as Selectable;
			//print("selectableBox focused? " + selectableBox.focused);
			//print("selectableBox selected? " + selectableBox.selected);
			if (selectableBox.selected){
				result=true;
				break;
			}
		} else if (layerIndex == 9) {
			var selectableAxis : SelectableAxis = targets[j].GetComponent(SelectableAxis) as SelectableAxis;
			//print("selectableAxis focused? " + selectableAxis.focused);
			//print("selectableAxis selected? " + selectableAxis.selected);

			if (selectableAxis.selected){
				result=true;
				break;
			}
		}
	}
	return result;
}

 static function anyAxisFocused(){
	var result = false;
	var layermask = LayerMask.GetMask("SelectableAxis");
	var targets = getSelectables(layermask);

	for (var j = 0; j < targets.Length; j++){
		var selectableAxis : SelectableAxis = targets[j].GetComponent(SelectableAxis) as SelectableAxis;
		//print("selectableAxis focused? " + selectableAxis.focused);
		//print("selectableAxis selected? " + selectableAxis.selected);
		if (selectableAxis.focused){
			result=true;
			break;
		}
	}
	return result;
}