﻿#pragma strict

var focused = false;
var selected = false;
var timeSelected = 0;

function Start () {
}

function Update () {
}

function focus(anySelected){
	focused = true;
	//print("focused called!!");
	if(!selected){
		paint(Color.blue);
    }
}

function unfocus(){
	focused = false;
	if(! selected){
		paint(Color.black);
    }
}

function ApproveSelection(){
	var anyAxisFocused = UsefulUtils.anyAxisFocused();

	if(focused){
		selected = true;
		paint(Color.green);
        //rend.material.SetColor("Color", Color.green);
        /*
        var rgBody = gameObject.GetComponent.<Rigidbody>();
        rgBody.useGravity = false;
        rgBody.isKinematic = true;
        */
        timeSelected = Time.time;
	} else if (anyAxisFocused) {
		Deselect();
	}
}

function paint(color : Color){
	var rend = transform.GetComponent.<Renderer>();
	rend.material.color = color;
}

function Deselect(){
//	print("Axis deselected");
	selected = false;
	//var rgBody = gameObject.GetComponent.<Rigidbody>();
    //rgBody.useGravity = true;
   	//rgBody.isKinematic = false;
}