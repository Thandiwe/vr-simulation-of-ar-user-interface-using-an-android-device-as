﻿#pragma strict

var layermask : LayerMask;

function Start () {
	// Makes sure the selectableAxis objects don collide with their respective selectables(bricks).
	Physics.IgnoreLayerCollision(8,9);
}

function Update () {
	ResetHighlights();
	// check if some object has been positively selected, if so don't preHighlight any other objects
	PrehighlightObjectInCenter();

	if((Input.touches.length > 0) && Input.touches[0].tapCount == 2){
		if(UsefulUtils.anySelected(layermask)){
			Deselect();
		}
	} else if((Input.touches.length > 0) && Input.touches[0].tapCount == 1){
		var touch = Input.touches[0];
		switch(touch.phase){
			case TouchPhase.Began:
				ApproveSelection();
//				print(touch.phase);
		}
	}
}



function PrehighlightObjectInCenter(){
	var hit : RaycastHit;
 	var cameraCenter = GetComponent.<Camera>().ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height / 2f, GetComponent.<Camera>().nearClipPlane));
	var fwd: Vector3 = transform.TransformDirection(Vector3.forward);
	var targets = UsefulUtils.getSelectables(layermask);

	var anySelected = UsefulUtils.anySelected(layermask);

	if(Physics.Raycast(transform.position, fwd, hit, 10000000, layermask)){
		var objectInCenter = hit.transform;
		//print("Object in center: " + objectInCenter);
		objectInCenter.SendMessage("focus", anySelected);
	}
}

function ResetHighlights(){
	var targets = UsefulUtils.getSelectables(layermask);
	//print("Targets size: " + len(targets));
	for (var i = 0; i < targets.Length; i++) {
		targets[i].SendMessage("unfocus");
	}
}


function ApproveSelection(){
	var targets = UsefulUtils.getSelectables(layermask);
	var anySelected = UsefulUtils.anySelected(layermask);

	for (var i = 0; i < targets.Length; i++) {
		targets[i].SendMessage("ApproveSelection", anySelected);
	}
}


function Deselect(){
	var targets = UsefulUtils.getSelectables(layermask);
	var layerIndex = Mathf.RoundToInt(Mathf.Log(layermask.value, 2));
	//print("Layerindex is: " + layerIndex);

	for (var j = 0; j < targets.Length; j++){
		if (layerIndex == 8){
			var script : Selectable = targets[j].GetComponent(Selectable) as Selectable;
			if (script.selected){
				targets[j].SendMessage("Deselect");
				break;
			}
		} else if(layerIndex == 9) {
			var script2 : SelectableAxis = targets[j].GetComponent(SelectableAxis) as SelectableAxis;
			if (script2.selected){
				targets[j].SendMessage("Deselect");
				break;
			}
		}
	}
}