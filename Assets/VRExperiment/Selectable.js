﻿#pragma strict

var focused = false;
var selected = false;
var selectedTime = 0.0f;
var selectedCounter = 0;

var arrowTemplate : GameObject;

var arrow_x : GameObject;
// arrow_x : up
var arrow_y : GameObject;
// arrow_y : 
var arrow_z : GameObject;

var startObjectCoords : Vector3;
var startTouchCoords : Vector2;

var updateTimer : boolean;

var rotationApplied = Vector3(0, 0, 0);

var lastPositionalDev = Vector3(-1000, -1000, -1000);
var lastRotationalDev = Vector3(-1000, -1000, -1000);

function Start () {
	
}

function Update () {

	var xArrow : SelectableAxis;
	var yArrow : SelectableAxis;
	var zArrow : SelectableAxis;
	
	//One-Finger Touch --> select axis for translation / calculate diff during swipe for translation
	if(selected && Input.touches.length == 1){
		var touch = Input.touches[0];
		switch(touch.phase){
			case TouchPhase.Moved:


		//if(touch.phase.Began){
			//print("Touch-Began");
			//startObjectCoords = transform.position;
			//startTouchCoords = touch.position;
		//} else if(touch.phase.Moved) {

			xArrow = arrow_x.GetComponent(SelectableAxis) as SelectableAxis;
			yArrow = arrow_y.GetComponent(SelectableAxis) as SelectableAxis;
			zArrow = arrow_z.GetComponent(SelectableAxis) as SelectableAxis;
			
			//print("Touch-Moved");
			// change of touch coordinate on screen.
			var del = touch.deltaPosition.y * 0.001;


			if(xArrow.selected){
				//print("changing X coordinate.");

				transform.position += del * arrow_x.transform.forward;
				arrow_x.transform.position += del * arrow_x.transform.forward;
				arrow_y.transform.position += del * arrow_x.transform.forward;
				arrow_z.transform.position += del * arrow_x.transform.forward;
			} else if(yArrow.selected){
				//print("changing Y coordinate.");
				transform.position += del * arrow_y.transform.forward;
				arrow_x.transform.position += del * arrow_y.transform.forward;
				arrow_y.transform.position += del * arrow_y.transform.forward;
				arrow_z.transform.position += del * arrow_y.transform.forward;
			} else if(zArrow.selected){
				//print("changing Z coordinate.");
				transform.position += del * arrow_z.transform.forward;
				arrow_x.transform.position += del * arrow_z.transform.forward;
				arrow_y.transform.position += del * arrow_z.transform.forward;
				arrow_z.transform.position += del * arrow_z.transform.forward;
			}
			break;
		}
	} else if(selected && Input.touches.length == 2){
		//print("DoubleTouch detected");
		Input.gyro.enabled = true;

		xArrow = arrow_x.GetComponent(SelectableAxis) as SelectableAxis;
		yArrow = arrow_y.GetComponent(SelectableAxis) as SelectableAxis;
		zArrow = arrow_z.GetComponent(SelectableAxis) as SelectableAxis;

		if(xArrow.selected){
		    
			transform.Rotate(transform.forward, -Input.gyro.rotationRateUnbiased.x, 0);
			yArrow.transform.Rotate(transform.forward, -Input.gyro.rotationRateUnbiased.x, 0);
			zArrow.transform.Rotate(transform.forward, -Input.gyro.rotationRateUnbiased.x, 0);
			rotationApplied.y += Math.Abs(Input.gyro.rotationRateUnbiased.x);
		}
		if(zArrow.selected){
		    
			transform.Rotate(transform.right, -Input.gyro.rotationRateUnbiased.x, 0);
			xArrow.transform.Rotate(transform.right, -Input.gyro.rotationRateUnbiased.x, 0);
			yArrow.transform.Rotate(transform.right, -Input.gyro.rotationRateUnbiased.x, 0);
			rotationApplied.z += Math.Abs(Input.gyro.rotationRateUnbiased.x);
		}
		if(yArrow.selected){
		    
			transform.Rotate(transform.up, -Input.gyro.rotationRateUnbiased.x, 0);
			xArrow.transform.Rotate(transform.up, -Input.gyro.rotationRateUnbiased.x, 0);
			zArrow.transform.Rotate(transform.up, -Input.gyro.rotationRateUnbiased.x, 0);
			rotationApplied.x += Math.Abs(Input.gyro.rotationRateUnbiased.x);
		}
		print(rotationApplied);
		//transform.Rotate(transform.forward, -Input.gyro.rotationRateUnbiased.y, 0);
		//transform.Rotate(transform.right, -Input.gyro.rotationRateUnbiased.x, 0);
		//transform.Rotate(transform.up, -Input.gyro.rotationRateUnbiased.z, 0);
		//transform.rotation = attitude;

	}
	
	// Record the interaction time.
	if (updateTimer) {
      selectedTime += Time.deltaTime;
	}
}



function focus(anySelected){
	focused = true;
	if(! anySelected){
		var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.yellow;
    }
}

function unfocus(){
	focused = false;
	if(! selected){
		var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.white;
    }
}

function ApproveSelection(anySelected){
	if(focused && (!anySelected)){
		selected = true;
		var rend = gameObject.GetComponent.<Renderer>();
        rend.material.color = Color.magenta;
        var rgBody = gameObject.GetComponent.<Rigidbody>();
        rgBody.useGravity = false;
        rgBody.isKinematic = true;


        drawTranslationArrows();
        
		selectedCounter += 1;
		updateTimer = true;

	}
}

function Deselect(){
	selected = false;
	updateTimer = false;
	var rgBody = gameObject.GetComponent.<Rigidbody>();
    rgBody.useGravity = true;
   	rgBody.isKinematic = false;
   	destroyTranslationArrows();
	
    //calculate the distance and rotational deviation in relation to ghost
   	var targetCollision = gameObject.GetComponent.<TargetCollision>();
	var deviations = targetCollision.trigger();
	lastPositionalDev = deviations[0];
	lastRotationalDev = deviations[1];
	print("Positional deviation: " + lastPositionalDev);
	print("Rotational deviation: " + lastRotationalDev);
	print("Total rotation: " + rotationApplied);
	//TODO: record the deviations for analysis
}


function drawTranslationArrows(){
	arrow_x = spawnArrow('x');
	arrow_y = spawnArrow('y');
	arrow_z = spawnArrow('z');
}

function destroyTranslationArrows(){
	Destroy(arrow_x);
	Destroy(arrow_y);
	Destroy(arrow_z);
}


function spawnArrow(direction){
	var arrow = Instantiate(arrowTemplate);
	arrow.transform.position = transform.position;

	if(direction == 'x'){
		arrow.transform.forward = transform.forward;
		//arrow.transform.up = Vector3(1,0,0);
	}


	if(direction == 'y'){
		arrow.transform.forward = transform.up;
		//arrow.transform.up = Vector3(0,1,0);
	}


	if(direction == 'z'){
		arrow.transform.forward = -transform.right;
		//arrow.transform.up = Vector3(0,0,1);
	}


	//arrow.transform.parent = transform;
	arrow.layer = LayerMask.NameToLayer("SelectableAxis"); //layer index for SelectableAxis
	var sa: SelectableAxis = arrow.AddComponent.<SelectableAxis>() as SelectableAxis;
	var re: MeshRenderer = arrow.AddComponent.<MeshRenderer>() as MeshRenderer;
	//var co: MeshCollider = arrow.AddComponent.<MeshCollider>() as MeshCollider;

	return arrow;

}

function retrieveResults(){
    var results = [gameObject.name, lastPositionalDev, lastRotationalDev, rotationApplied, selectedCounter, selectedTime];
    var keys = ["game object", "positional deviation", "rotational deviation", "rotation applied", "object selected times", "total object selected time"];
    for (var i = 0; i<results.length; i++){
        print(keys[i] + ": " + results[i]);
    }
    return results;
}