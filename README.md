# README #

### AR User Interfaces-Android ###

This project is for AR User Interfaces interaction simulation in Virtual Reality using an android device as an input device. Object interaction in the VR world is facilitated by a swipe gesture(for translation)on the android,double tap for selecting and deselecting as well as both thumbs down and rotating the device for rotation. 
### Getting Started ###
The following will be required:

* Unity version  5.5.0  
* HTC Vive
* android device with android Lollipop (5.0) or higher
* Unity remote 5 installed on the android device
	The installation procedure of Unity Remote 5 is explain on the Unity page https://docs.unity3d.com/Manual/UnityRemote5.html


### Contact ###
Thandiwe Feziwe Mangana

*  Email: feziwemangana@gmail.com